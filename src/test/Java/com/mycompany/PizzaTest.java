package com.mycompany;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import java.io.IOException;
import java.util.*;
import java.util.logging.LogManager;

public class PizzaTest extends TestCase {
    /**
     * @param testName - The name of a test.
     */
    public PizzaTest(String testName) {
        super(testName);
    }

    public void addSameIngredientsTest() throws IOException {
        IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
        Map<String, Ingredient> ingredients = ingredientRepository.getAll();
        clearIngredients(ingredients);

        Pizza newPizza = new Pizza("Composite");
        try {
            List<Ingredient> ingredientList = new ArrayList<>(ingredients.values());

            Ingredient newIngredient = ingredientList.get(0).clone();
            newIngredient.add(2f);
            newPizza.addIngredient(newIngredient);

            Ingredient sameIngredient = ingredientList.get(0).clone();
            sameIngredient.add(1f);
            newPizza.addIngredient(sameIngredient);

            Ingredient anotherIngredient = ingredientList.get(1).clone();
            anotherIngredient.add(4f);
            newPizza.addIngredient(anotherIngredient);

            assertTrue(newPizza.getIngredients().size() == 2);

            Ingredient ingredient = newPizza.getIngredients().get(newIngredient.getName());
            assertTrue(ingredient.getQuantity() == 3f ||
                    ingredient.getQuantity() == ingredient.getMaxQuantityPerPizza());
        }
        catch (CloneNotSupportedException exception) {
            System.out.println(exception.getMessage());
        }
        System.out.println("addSameIngredientsTest: Ok.");
    }

    public void cloneTest() throws IOException {
        IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
        Map<String, Ingredient> storageIngredients = ingredientRepository.getAll();

        Pizza originalPizza = new Pizza("Test");
        originalPizza.setIngredients(storageIngredients);
        try {
            Pizza newPizza = originalPizza.clone();

            Collection<Ingredient> ingredients = newPizza.getIngredients().values();
            Ingredient ingredient = ingredients.iterator().next();
            ingredient.add(2f);

            Ingredient originalIngredient = originalPizza.getIngredients().get(ingredient.getName());

            assertTrue(originalIngredient.getQuantity() == 0f);
            assertTrue(ingredient.getQuantity() == 2f ||
                    ingredient.getQuantity() == ingredient.getMaxQuantityPerPizza());
        }
        catch (CloneNotSupportedException exception) {
            System.out.println(exception.getMessage());
        }
        System.out.println("cloneTest: Ok.");
    }

    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logging.properties"));
        } catch (IOException exception) {
            System.err.println("Could not setup logger configuration: " + exception.toString());
        }

        TestRunner runner = new TestRunner();
        TestSuite suite = new TestSuite();
        suite.addTest(new PizzaTest("addSameIngredientsTest"));
        suite.addTest(new PizzaTest("cloneTest"));
        runner.doRun(suite);
    }

    private void clearIngredients(Map<String, Ingredient> ingredients) {
        for (Ingredient ingredient : ingredients.values()) {
            ingredient.add(-ingredient.getQuantity());
        }
    }
}
