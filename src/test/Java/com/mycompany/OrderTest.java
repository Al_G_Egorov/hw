package com.mycompany;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import java.io.IOException;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

public class OrderTest extends TestCase {
    public OrderTest(String testName) {
        super(testName);
    }

    public void ingredientsQuantityTest() {
        int testsNumber = (new Random()).nextInt(10);
        for (int testNumber = 0; testNumber < testsNumber; ++testNumber) {
            System.out.println(">>>>>>>>Test number #" + (testNumber + 1) + "<<<<<<<<<<<<");
            Map<String, Ingredient> ingredientsAtBeginning = getIngredientsCopy();
            List<Order> orders = makeSeveralOrders();
            Map<String, Ingredient> ingredientsAtEnding = getIngredientsCopy();
            checkIngredientsQuantity(ingredientsAtBeginning, orders, ingredientsAtEnding);
        }
        System.out.println("ingredientsQuantityTest: Ok.");
    }

    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logging.properties"));
        } catch (IOException exception) {
            System.err.println("Could not setup logger configuration: " + exception.toString());
        }

        TestRunner runner = new TestRunner();
        TestSuite suite = new TestSuite();
        suite.addTest(new OrderTest("ingredientsQuantityTest"));
        runner.doRun(suite);
    }

    private List<Order> makeSeveralOrders() {
        Chief chief = new Chief();
        Thread chiefThread = new Thread(chief);
        chiefThread.start();

        final int MAX_TIME_BETWEEN_TWO_ORDERS = 2_000;   // milliseconds
        List<Order> orders = new ArrayList<>();
        try {
            int clientsNumber = (new Random()).nextInt(10);
            Pizza pizza = new Pizza("test pizza");
            for (int clientNumber = 0; clientNumber < clientsNumber; ++clientNumber) {
                TimeUnit.MILLISECONDS.sleep((new Random()).nextInt(MAX_TIME_BETWEEN_TWO_ORDERS));
                orders.add(new Order("Name" + Integer.toString(clientNumber + 1), pizza));
                chief.takeOrder(orders.get(orders.size() - 1));
            }
            final int MAX_WORK_TIME = 5 * (MAX_TIME_BETWEEN_TWO_ORDERS + 1);   // milliseconds
            TimeUnit.MILLISECONDS.sleep(MAX_WORK_TIME);
        } catch (InterruptedException exception) {
            System.out.println(exception.getMessage());
        } finally {
            chiefThread.interrupt();
        }
        return orders;
    }

    private void checkIngredientsQuantity(final Map<String, Ingredient> ingredientsAtBeginning,
                                          final List<Order> orders,
                                          final Map<String, Ingredient> ingredientsAtEnding) {

        for (String name : ingredientsAtBeginning.keySet()) {
            float quantity = (ingredientsAtEnding.get(name) != null) ?
                    ingredientsAtEnding.get(name).getQuantity() : 0f;
            for (Order order : orders) {
                Pizza pizza = order.getPizza();
                Map<String, Ingredient> ingredients = pizza.getIngredients();
                if (ingredients.get(name) != null) {
                    quantity += ingredients.get(name).getQuantity();
                }
            }
            assertTrue((name + " " + quantity + " " + ingredientsAtBeginning.get(name).getQuantity()),
                    quantity == ingredientsAtBeginning.get(name).getQuantity());
        }
    }

    private Map<String, Ingredient> getIngredientsCopy() {
        Map<String, Ingredient> ingredientsCopy = new HashMap<>();
        try {
            IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
            Map<String, Ingredient> ingredients = ingredientRepository.getAll();
            for (String name : ingredients.keySet()) {
                ingredientsCopy.put(name, ingredients.get(name).clone());
            }
        } catch (CloneNotSupportedException exception) {
            System.out.println(exception.getMessage());
        }
        return ingredientsCopy;
    }
}
