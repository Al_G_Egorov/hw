package com.mycompany;

import java.io.IOException;
import java.util.Map;
import java.util.logging.LogManager;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class IngredientTest extends TestCase {
    public IngredientTest(String testName) {
        super(testName);
    }

    public void addIngredientTest() throws IOException {
        IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
        Map<String, Ingredient> ingredients = ingredientRepository.getAll();
        clearIngredients(ingredients);
        for (Ingredient ingredient : ingredients.values()) {
            try {
                Ingredient newIngredient = ingredient.clone();
                assertTrue(newIngredient.getQuantity() == 0f);
                newIngredient.add(2f);
                assertTrue(newIngredient.getQuantity() == 2f ||
                        (newIngredient.getQuantity() == newIngredient.getMaxQuantityPerPizza()));
                newIngredient.add(4f);
                assertTrue(newIngredient.getQuantity() == 6f ||
                        (newIngredient.getQuantity() == newIngredient.getMaxQuantityPerPizza()));
                newIngredient.add(newIngredient);
                assertTrue(newIngredient.getQuantity() == 12f ||
                        (newIngredient.getQuantity() == newIngredient.getMaxQuantityPerPizza()));
                assertTrue(ingredient.getQuantity() == 0f);
            }
            catch (CloneNotSupportedException exception) {
                System.err.println(exception.getMessage());
            }
        }
        System.out.println("addIngredientTest: Ok.");
    }

    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logging.properties"));
        } catch (IOException exception) {
            System.err.println("Could not setup logger configuration: " + exception.toString());
        }

        TestRunner runner = new TestRunner();
        TestSuite suite = new TestSuite();
        suite.addTest(new IngredientTest("addIngredientTest"));
        runner.doRun(suite);
    }

    private void clearIngredients(Map<String, Ingredient> ingredients) {
        for (Ingredient ingredient : ingredients.values()) {
            ingredient.add(-ingredient.getQuantity());
        }
    }
}
