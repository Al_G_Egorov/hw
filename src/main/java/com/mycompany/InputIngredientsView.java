package com.mycompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputIngredientsView extends View {
    public InputIngredientsView(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    public int showMenu() {
        do {
            System.out.println("1 - show ingredients storage;");
            System.out.println("2 - add ingredients.");

            try {
                Integer clientChoice = Integer.parseInt(bufferedReader.readLine());
                if (clientChoice == 1 || clientChoice == 2) {
                    return clientChoice;
                }
                System.out.println("Wrong value.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public boolean willContinue() {
        System.out.print("Do you want to exit? (y / n)");
        try {
            return !Objects.equals(bufferedReader.readLine(), "y");
        } catch (IOException exception) {
            System.out.println("Wrong value.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
        return false;
    }

    public void showIngredients(final Map<String, Ingredient> ingredients) {
        for (String name : ingredients.keySet()) {
            Ingredient ingredient = ingredients.get(name);
            System.out.print(ingredient.getName() + " " + ingredient.getQuantity());
            Unit unit = ingredient.getUnit();
            if (ingredient.getQuantity() == 1f) {
                System.out.println(" " + unit.getNameInSingular());
            } else {
                System.out.println(" " + unit.getNameInPlural());
            }
        }
        System.out.println();
    }

    public String inputIngredientName() {
        do {
            System.out.println("Input an ingredient name:");
            try {
                String name = bufferedReader.readLine();
                if (!name.trim().equals("")) {
                    return name;
                }
                System.out.println("Wrong value.");
            } catch (IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public Unit inputIngredientUnit(final Map<String, Unit> units) throws IOException {
        do {
            System.out.println("Select an ingredient unit:");

            List<Unit> unitsList = new ArrayList<>(units.values());
            for (int index = 0; index < unitsList.size(); ++index) {
                System.out.println(String.valueOf(index + 1) + " - " + unitsList.get(index).getNameInSingular());
            }
            try {
                Integer clientChoice = Integer.parseInt(bufferedReader.readLine());
                if (clientChoice >= 1 && clientChoice <= unitsList.size()) {
                    return unitsList.get(clientChoice - 1);
                }
                System.out.println("Wrong value.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public float inputQuantity(final Unit unit) {
        do {
            System.out.print("Type the quantity of the ingredient");
            if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                System.out.print(" (integer number)");
            }
            System.out.println(":");

            try {
                String answer = bufferedReader.readLine();
                float quantity;

                if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                    quantity = (float)Integer.parseInt(answer);
                } else {    // Unit.UnitType.FLOAT
                    quantity = Float.parseFloat(answer);
                }
                if (quantity >= 0) {
                    return quantity;
                }
                System.out.println("Wrong value.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public float inputMinQuantityPerPizza(final Unit unit) {
        do {
            System.out.print("Input min recommended quantity per one pizza");
            if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                System.out.print(" (integer number)");
            }
            System.out.println(":");

            try {
                String answer = bufferedReader.readLine();
                float minQuantity;

                if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                    minQuantity = (float)Integer.parseInt(answer);
                } else {    // Unit.UnitType.FLOAT
                    minQuantity = Float.parseFloat(answer);
                }
                if (minQuantity >= 0) {
                    return minQuantity;
                }
                System.out.println("Wrong value.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public float inputMaxQuantityPerPizza(final Unit unit, float minQuantityPerPizza) {
        do {
            System.out.print("Input max recommended quantity per one pizza");
            System.out.print("Type the quantity of the ingredient");
            if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                System.out.print(" (integer number)");
            }
            System.out.println(":");

            try {
                String answer = bufferedReader.readLine();
                float maxQuantity;

                if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                    maxQuantity = (float)Integer.parseInt(answer);
                } else {    // Unit.UnitType.FLOAT
                    maxQuantity = Float.parseFloat(answer);
                }
                if (maxQuantity > minQuantityPerPizza) {
                    return maxQuantity;
                }
                System.out.println("Wrong value.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    private BufferedReader bufferedReader;
    private static Logger log = Logger.getLogger(InputIngredientsView.class.getName());
}
