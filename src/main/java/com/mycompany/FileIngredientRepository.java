package com.mycompany;

import java.io.*;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileIngredientRepository implements IngredientRepository {
    FileIngredientRepository() {
        filePath = "data" + File.separator + "Storage";
        try {
            loadCollection();
        } catch (IOException exception) {
            System.out.println("Can't load collection from the file.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    FileIngredientRepository(final String filePath) {
        this.filePath = filePath;
        try {
            loadCollection();
        } catch (IOException exception) {
            System.out.println("Can't load collection from the file.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public synchronized void insert(final Ingredient ingredient) {
        ingredients.put(ingredient.getName(), ingredient);
        try {
            saveCollections();
        } catch (IOException exception) {
            ingredients.remove(ingredient.getName());   // rollback.

            System.out.println("Can't insert to the file.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public synchronized void update(final Ingredient ingredient) {
        Ingredient oldIngredientValue = ingredients.get(ingredient.getName());
        ingredients.put(ingredient.getName(), ingredient);
        try {
            saveCollections();
        } catch (IOException exception) {
            ingredients.put(ingredient.getName(), oldIngredientValue);

            System.out.println("Can't update the file.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public synchronized void delete(final String name) {
        Ingredient oldIngredientValue;
        try {
//            System.out.println("delete>>>>>>>>>" + name);
//            for (Ingredient ingredient : ingredients.values()) {
//                System.out.println(ingredient.getName());
//            }
            oldIngredientValue = ingredients.get(name).clone();
        } catch (CloneNotSupportedException exception) {
            System.out.println("Can't delete the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
            return;
        }

        ingredients.remove(oldIngredientValue.getName());

        try {
            saveCollections();
        } catch (IOException exception) {
            ingredients.put(oldIngredientValue.getName(), oldIngredientValue);

            System.out.println("Can't delete the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public synchronized Ingredient get(final String name) {
        try {
            return ingredients.get(name).clone();
        } catch (CloneNotSupportedException exception) {
            System.out.println("Can't get the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
            return null;
        }
    }

    @Override
    public synchronized Map<String, Ingredient> getAll() {
        return ingredients;
    }

    @Override
    public synchronized Map<String, Unit> getUnits() {
        return units;
    }

    @SuppressWarnings("unchecked")
    private synchronized void loadCollection() throws IOException {
        try (FileInputStream fileInput = new FileInputStream(filePath);
             ObjectInputStream inputStream = new ObjectInputStream(fileInput)) {

            Object object = inputStream.readObject();
            if (object instanceof Map) {
                ingredients = (Map<String, Ingredient>) object;
            }

            object = inputStream.readObject();
            if (object instanceof Map) {
                units = (Map<String, Unit>) object;
            }
        } catch (IOException | ClassNotFoundException exception) {
            System.out.println("Can't read file: " + filePath);
            log.log(Level.SEVERE, "Exception:", exception);

            File file = new File(filePath);
            if (file.exists()) {
                if (file.delete()) {
                    createNewFile();
                } else {
                    throw new IOException("Can't delete file: " + filePath);
                }
            } else {
                createNewFile();
            }
        }
    }

    private synchronized void createNewDirectory(final File directory) throws IOException {
        if (directory.mkdirs()) {
            System.out.println("New directory: " + directory.getAbsolutePath() + " has been created.");
        } else {
            throw new IOException("Can't create a new directory: " + directory.getAbsolutePath());
        }
    }

    private synchronized void createNewFile() throws IOException {
        File storageFile = new File(filePath);

        File directory = storageFile.getParentFile();
        if (!directory.exists()) {
            System.out.println("Directory: " + directory.getAbsolutePath() + " doesn't exist.");
            createNewDirectory(directory);
        }

        if (storageFile.createNewFile()) {
            IngredientRepository collectionIngredientRepository = new CollectionIngredientRepository();
            ingredients = collectionIngredientRepository.getAll();
            units = collectionIngredientRepository.getUnits();
            saveCollections();
            System.out.println("New file: " + storageFile.getAbsolutePath() + " has been created.");
        } else {
            throw new IOException("Can't create a new file: " + storageFile.getAbsolutePath());
        }
    }

    private synchronized void saveCollections() throws IOException  {
        try (FileOutputStream fileOutput = new FileOutputStream(filePath);
             ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput)) {

            outputStream.writeObject(ingredients);
            outputStream.writeObject(units);
        }
    }

    private Map<String, Ingredient> ingredients;
    private Map<String, Unit> units;

    private String filePath;
    private static Logger log = Logger.getLogger(FileIngredientRepository.class.getName());
}
