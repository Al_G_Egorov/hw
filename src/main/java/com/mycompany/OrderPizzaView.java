package com.mycompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static junit.framework.TestCase.assertTrue;

public class OrderPizzaView extends View {
    OrderPizzaView(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    public void showOrder(final Order order) {
        System.out.println();
        System.out.println("=======================Order===========================");
        System.out.println("The client name: " + order.getClientName());
        System.out.println("The order number: " + (order.getNumber() + 1));

        Pizza pizza = order.getPizza();
        System.out.println("Pizza:" + pizza.getName());
        System.out.println("Size:" + pizza.getSize());

        Map<String, Ingredient> ingredients = pizza.getIngredients();
        for (Ingredient ingredient : ingredients.values()) {
            System.out.print(ingredient.getName());

            Float quantity = ingredient.getQuantity();
            if (ingredient.getUnit().getType() == Unit.UnitType.EMPTY ||
                    ingredient.getUnit().getType() == Unit.UnitType.INTEGER) {
                System.out.print(" ");
                System.out.print(Math.round(quantity));
            } else if (ingredient.getUnit().getType() == Unit.UnitType.FLOAT) {
                System.out.print(" ");
                System.out.printf("%.3f", quantity);
            }

            if (ingredient.getQuantity() == 1f) {
                System.out.print(" " + ingredient.getUnit().getNameInSingular());
            } else {
                System.out.print(" " + ingredient.getUnit().getNameInPlural());
            }
            System.out.println();
        }
        System.out.println("=======================================================");
        System.out.println();
    }

    public int inputClientsNumber() {
        do {
            try {
                System.out.println("Input clients number:");
                int clientsNumber = Integer.parseInt(bufferedReader.readLine());
                if (clientsNumber >= 0) {
                    return clientsNumber;
                }
            } catch (IOException exception) {
                System.out.println("Input / output error.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public String inputClientName() {
        do {
            System.out.println("Type your name:");
            try {
                String name = bufferedReader.readLine();
                if (!name.trim().isEmpty()) {
                    return name;
                }
                System.out.println("Wrong value.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    public void print(final String text) {
        System.out.println(text);
    }

    public void showIngredients(final List<String> ingredientsNames) {
        System.out.println("Choose an ingredient:");
        for (Integer index = 0; index < ingredientsNames.size(); ++index) {
            System.out.println(String.valueOf(index + 1) + " - " + ingredientsNames.get(index));
        }
        System.out.println();
    }

    public void showIngredients(final Map<String, Ingredient> ingredients) {
        System.out.println("++++++++++++++++Ingredients++++++++++++++++");
        for (String name : ingredients.keySet()) {
            System.out.println(name + " " + ingredients.get(name).getQuantity());
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
    }

    public String inputIngredient(final List<String> ingredientsNames) {
        do {
            System.out.println("Type the number of an ingredient:");
            try {
                String answer = bufferedReader.readLine();
                if (answer.toLowerCase().equals("help") || answer.equals("?")) {
                    System.out.println("Input the number.");
                    continue;
                }
                Integer clientChoice = Integer.parseInt(answer);
                if (clientChoice >= 1 && clientChoice <= ingredientsNames.size()) {
                    return ingredientsNames.get(clientChoice - 1);
                }
                System.out.println("Wrong value!");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
            }
        } while (true);
    }

    public String inputIngredientRandom(final List<String> ingredientsNames) {
        assertTrue(ingredientsNames.size() > 0);
        return ingredientsNames.get((new Random()).nextInt(ingredientsNames.size()));
    }

    public float inputIngredientQuantity(final Ingredient ingredientFrom,
                                         float currentQuantity,
                                         float minQuantityValue,
                                         float maxQuantityValue) {//-------------------------------------
        Unit unit = ingredientFrom.getUnit();
        do {
            System.out.print("Type the quantity of the ingredient");
            if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                System.out.print(" (integer number)");
            }

            System.out.print(". Current value: " + currentQuantity);
            System.out.print(", min value: " + minQuantityValue);
            System.out.println(", max value: " + maxQuantityValue + ".");

            try {
                String answer = bufferedReader.readLine();
                if (answer.toLowerCase().equals("help") || answer.equals("?")) {
                    System.out.println("Input the number.");
                    continue;
                }

                float quantity = (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) ?
                        Integer.parseInt(answer) : Float.parseFloat(answer);
                if (currentQuantity + quantity >= minQuantityValue && currentQuantity + quantity <= maxQuantityValue) {
                    return quantity;
                }
                System.out.println("Wrong number.");
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong number.");
            }
        } while (true);
    }

    public boolean inputWillContinue() {
        System.out.print("Do you want to type the next ingredient? (y / n)");
        try {
            return Objects.equals(bufferedReader.readLine(), "y");
        } catch (IOException exception) {
            System.out.println("Wrong input.");
            return false;
        }
    }

    public int inputPizzaSize(boolean isRandom) {
        final List<Integer> allowedSizes = Arrays.asList(30, 40, 50);
        if (isRandom) {
            return allowedSizes.get((new Random()).nextInt(allowedSizes.size()));
        }

        do {
            System.out.println("Which the pizza size (30/40/50) do you prefer?");
            try {
                String answer = bufferedReader.readLine();
                if (answer.toLowerCase().equals("help") || answer.equals("?")) {
                    System.out.println("Input the number.");
                    continue;
                }
                Integer clientChoice = Integer.parseInt(answer);
                if (allowedSizes.contains(clientChoice)) {
                    return clientChoice;
                }
                System.out.println("Wrong value.");
            } catch(NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
            }
        } while (true);
    }

    private BufferedReader bufferedReader;
    private static Logger log = Logger.getLogger(MenuView.class.getName());
}
