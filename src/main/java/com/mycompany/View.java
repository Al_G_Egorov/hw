package com.mycompany;

import java.util.ArrayList;
import java.util.List;

public abstract class View {
    public View() {
        listeners = new ArrayList<>();
    }

    public void addEventListener(final Listener listener) {
        listeners.add(listener);
    }

    public void fireEvent(final Events event) {
        for (Listener listener: listeners) {
            listener.onEvent(event);
        }
    }

    protected List<Listener> listeners;
}
