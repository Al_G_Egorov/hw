package com.mycompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuView extends View {
    public MenuView(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    public void show() {
        do {
            System.out.println("Select the operating mode:");
            System.out.println("1 - make a pizza order;");
            System.out.println("2 - work with the storage;");
            System.out.println("3 - emulate several clients;");
            System.out.println("4 - exit.");
            try {
                Integer clientChoice = Integer.parseInt(bufferedReader.readLine());
                if (clientChoice == 1) {
                    fireEvent(Events.ORDER_PIZZA);
                    return;
                } else if (clientChoice == 2) {
                    fireEvent(Events.INPUT_INGREDIENTS);
                    return;
                } else if (clientChoice == 3) {
                    fireEvent(Events.ORDER_RANDOM_PIZZAS);
                    return;
                } else if (clientChoice == 4) {
                    fireEvent(Events.EXIT);
                    return;
                } else {
                    System.out.println("Wrong value.");
                }
            } catch (NumberFormatException | IOException exception) {
                System.out.println("Wrong value.");
                log.log(Level.SEVERE, "Exception:", exception);
            }
        } while (true);
    }

    private BufferedReader bufferedReader;
    private static Logger log = Logger.getLogger(MenuView.class.getName());
}
