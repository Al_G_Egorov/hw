package com.mycompany;

public class Unit implements Cloneable, java.io.Serializable {
    public Unit(final String nameInSingular, final String nameInPlural, final UnitType type) {
        this.nameInSingular = nameInSingular;
        this.nameInPlural = nameInPlural;
        this.type = type;
    }

    String getNameInPlural() {
        return nameInPlural;
    }

    String getNameInSingular() {
        return nameInSingular;
    }

    UnitType getType() {
        return type;
    }

    protected Unit clone() throws CloneNotSupportedException {
        return (Unit) super.clone();
    }

    enum UnitType { INTEGER, FLOAT, EMPTY }

    private UnitType type;
    private String nameInSingular;
    private String nameInPlural;
}
