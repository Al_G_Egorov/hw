package com.mycompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputIngredientsService extends Service {
    public InputIngredientsService(final BufferedReader bufferedReader) {
        inputIngredientsView = new InputIngredientsView(bufferedReader);
    }

    public void showInputIngredientsMenu() {
        IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
        do {
            int clientChoice = inputIngredientsView.showMenu();
            if (clientChoice == 1) {
                inputIngredientsView.showIngredients(ingredientRepository.getAll());
            } else if (clientChoice == 2) {
                try {
                    inputIngredient();
                } catch (IOException exception) {
                    System.out.println("An input / output error.");
                    log.log(Level.SEVERE, "Exception:", exception);
                }
            }
        } while (inputIngredientsView.willContinue());

        fireEvent(Events.EXIT);
    }

    private void inputIngredient() throws IOException {
        String name = inputIngredientsView.inputIngredientName();

        IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
        Map<String, Unit> units = ingredientRepository.getUnits();

        Ingredient oldIngredient = ingredientRepository.get(name);
        if (oldIngredient == null) {
            Unit unit = inputIngredientsView.inputIngredientUnit(units);
            float quantity = inputIngredientsView.inputQuantity(unit);
            float minQuantityPerPizza = inputIngredientsView.inputMinQuantityPerPizza(unit);
            float maxQuantityPerPizza = inputIngredientsView.inputMaxQuantityPerPizza(unit, minQuantityPerPizza);

            Ingredient ingredient = new Ingredient(name,
                    quantity,
                    minQuantityPerPizza,
                    maxQuantityPerPizza,
                    unit);
            ingredientRepository.insert(ingredient);
        } else {
            Unit unit = oldIngredient.getUnit();
            float quantity = inputIngredientsView.inputQuantity(unit);
            float minQuantityPerPizza = oldIngredient.getMinQuantityPerPizza();
            float maxQuantityPerPizza = oldIngredient.getMaxQuantityPerPizza();

            Ingredient ingredient = new Ingredient(name,
                    oldIngredient.getQuantity() + quantity,
                    minQuantityPerPizza,
                    maxQuantityPerPizza,
                    unit);

            ingredientRepository.update(ingredient);
        }
    }

    private InputIngredientsView inputIngredientsView;
    private static Logger log = Logger.getLogger(PizzaOrderService.class.getName());
}
