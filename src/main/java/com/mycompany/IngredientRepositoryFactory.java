package com.mycompany;

public class IngredientRepositoryFactory {
    protected static synchronized IngredientRepository setRepository(
            final IngredientRepository newIngredientRepository) {
        IngredientRepository old = ingredientRepository;
        ingredientRepository = newIngredientRepository;
        return old;
    }

    public static synchronized IngredientRepository get() {
        return ingredientRepository == null ?
                ingredientRepository = new PostgresqlIngredientRepository() : ingredientRepository;
    }

    private static IngredientRepository ingredientRepository = null;
}
