package com.mycompany;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MenuController implements Listener {
//    public static MenuController getInstance() {
//        if (menuController == null) {
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//            MenuView menuView = new MenuView(bufferedReader);
//            menuController = new MenuController(menuView);
//            menuView.addEventListener(menuController);
//        }
//        return menuController;
//    }

    public MenuController(final MenuView menuView, final PizzaOrderService pizzaOrderService,
                          final InputIngredientsService inputIngredientsService) {
        this.menuView = menuView;
        this.pizzaOrderService = pizzaOrderService;
        this.inputIngredientsService = inputIngredientsService;
    }

    public void onEvent(Events event) {
        if (event.equals(Events.SHOW_MENU)) {
            menuView.show();
        } else if (event.equals(Events.ORDER_PIZZA)) {
            pizzaOrderService.takeOrder();
        } else if (event.equals(Events.INPUT_INGREDIENTS)) {
            inputIngredientsService.showInputIngredientsMenu();
        } else if (event.equals(Events.ORDER_RANDOM_PIZZAS)) {
            pizzaOrderService.emulateClientsSteam();
        } else if (event.equals(Events.EXIT)) {
            pizzaOrderService.close();
        }
    }

    private MenuView menuView;
    private PizzaOrderService pizzaOrderService;
    private InputIngredientsService inputIngredientsService;
}
