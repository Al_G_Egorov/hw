package com.mycompany;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import org.postgresql.ds.PGPoolingDataSource;


public class PostgresqlIngredientRepository implements IngredientRepository {
    public PostgresqlIngredientRepository() {
        try {
            Properties props = new Properties();
            props.load(PostgresqlIngredientRepository.class.getResourceAsStream("/PostgresqlConnection.properties"));

            String uri = props.getProperty("uri", "127.0.0.1:5432");
            String database = props.getProperty("database", "mydb");
            String username = props.getProperty("username", "alex");
            String password = props.getProperty("password", "332211");
            int maxConnections = Integer.parseInt(props.getProperty("max_connections", "100"));
            int initialConnections = Integer.parseInt(props.getProperty("initial_connections", "20"));

            dataSource = new PGPoolingDataSource();
            dataSource.setServerName(uri);
            dataSource.setDatabaseName(database);
            dataSource.setUser(username);
            dataSource.setPassword(password);
            dataSource.setMaxConnections(maxConnections);
            dataSource.setInitialConnections(initialConnections);
        } catch (IOException | NullPointerException exception) {
            System.out.println("An input / output exception.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public void insert(final Ingredient ingredient) {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            String sql = "INSERT INTO ingredients " +
                    "(name, quantity, min_quantity_per_pizza, max_quantity_per_pizza, unit_id)" +
                    " VALUES ('" + ingredient.getName() + "', " +
                    +ingredient.getQuantity() + ", " +
                    +ingredient.getMinQuantityPerPizza() + ", " +
                    +ingredient.getMaxQuantityPerPizza() +
                    ", (SELECT id FROM units WHERE name_in_singular like '" +
                    ingredient.getUnit().getNameInSingular() + "'));";
            int result = statement.executeUpdate(sql);
            assertTrue(result > 0);
            connection.close();
        } catch (SQLException exception) {
            System.out.println("Can't insert the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public void update(final Ingredient ingredient) {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            String sql = "UPDATE ingredients " +
                    "SET" +
                    " quantity = " + ingredient.getQuantity() + "," +
                    " min_quantity_per_pizza = " + ingredient.getMinQuantityPerPizza() + "," +
                    " max_quantity_per_pizza = " + ingredient.getMaxQuantityPerPizza() + "," +
                    " unit_id = (" +
                    "SELECT id FROM units WHERE name_in_singular like '" +
                    ingredient.getUnit().getNameInSingular() + "')" +
                    "WHERE name = '" + ingredient.getName() + "';";

            int result = statement.executeUpdate(sql);
            assertTrue(result > 0);
            connection.close();
        } catch (SQLException exception) {
            System.out.println("Can't update the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public void delete(final String ingredientName) {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            String sql = "DELETE FROM ingredients " +
                    " WHERE name = '" + ingredientName + "';";
            int result = statement.executeUpdate(sql);
            assertTrue(result > 0);
            connection.close();
        } catch (SQLException exception) {
            System.out.println("Can't delete the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    @Override
    public Ingredient get(final String name) {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT " +
                    "id, name, quantity, min_quantity_per_pizza, max_quantity_per_pizza" +
                    ", (SELECT name_in_singular from units where id = unit_id) as unit_name_in_singular " +
                    " FROM ingredients" +
                    " WHERE name = '" + name + "';";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                Ingredient ingredient = new Ingredient(resultSet.getString("name"),
                        resultSet.getFloat("quantity"),
                        resultSet.getFloat("min_quantity_per_pizza"),
                        resultSet.getFloat("max_quantity_per_pizza"),
                        getUnit(resultSet.getString("unit_name_in_singular")));
                statement.close();
                connection.close();
                return ingredient;
            }
        } catch (SQLException exception) {
            System.out.println("Can't get the ingredient.");
            log.log(Level.SEVERE, "Exception:", exception);
        }
        return null;
    }

    @Override
    public Map<String, Ingredient> getAll() {
        Map<String, Unit> units = getUnits();
        return getAll(units);
    }

    @Override
    public Map<String, Unit> getUnits() {
        List<Unit> unitsList = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT " +
                    "id, name_in_singular, name_in_plural, type " +
                    "FROM units";
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                unitsList.add(new Unit(rs.getString("name_in_singular"), rs.getString("name_in_plural"),
                        Unit.UnitType.valueOf(rs.getString("type").toUpperCase())));
            }
            statement.close();
            connection.close();
        } catch (SQLException exception) {
            System.out.println("An input / output exception");
            log.log(Level.SEVERE, "Exception:", exception);
        }

        return unitsList.stream().collect(
                Collectors.toMap(Unit::getNameInSingular, Function.identity(), (a, b) -> a, TreeMap::new));
    }

    private Map<String, Ingredient> getAll(final Map<String, Unit> units) {
        List<Ingredient> ingredientsList = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            String sql = "SELECT " +
                    "id, name, quantity, min_quantity_per_pizza, max_quantity_per_pizza" +
                    ", (SELECT name_in_singular from units where id = unit_id) as unit_name_in_singular " +
                    "FROM ingredients";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                ingredientsList.add(new Ingredient(resultSet.getString("name"),
                        resultSet.getFloat("quantity"),
                        resultSet.getFloat("min_quantity_per_pizza"),
                        resultSet.getFloat("max_quantity_per_pizza"),
                        units.get(resultSet.getString("unit_name_in_singular"))));
            }
            statement.close();
            connection.close();
        } catch (SQLException exception) {
            System.out.println("An input / output exception");
            log.log(Level.SEVERE, "Exception:", exception);
        }

        return ingredientsList.stream().collect(
                Collectors.toMap(Ingredient::getName, Function.identity(), (a, b) -> a, TreeMap::new));
    }

    private Unit getUnit(final String name) throws SQLException{
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        String sql = "SELECT id, name_in_singular, name_in_plural, type" +
                " FROM units where name_in_singular = '" + name + "';";
        ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next()) {
            throw new SQLException("Can't get unit:" + name);
        }
        Unit unit = new Unit(resultSet.getString("name_in_singular"),
                resultSet.getString("name_in_plural"),
                Unit.UnitType.valueOf(resultSet.getString("type").toUpperCase()));
        statement.close();
        connection.close();
        return unit;
    }

    private PGPoolingDataSource dataSource;
    private static Logger log = Logger.getLogger(PostgresqlIngredientRepository.class.getName());
}
