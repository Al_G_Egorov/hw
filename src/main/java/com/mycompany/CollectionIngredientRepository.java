package com.mycompany;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectionIngredientRepository implements IngredientRepository {
    CollectionIngredientRepository() {
        UnitsInitialisation();
        IngredientsInitialisation();
    }

    @Override
    public void insert(final Ingredient ingredient) {
        ingredients.put(ingredient.getName(), ingredient);
    }

    @Override
    public void update(final Ingredient ingredient) {
        ingredients.put(ingredient.getName(), ingredient);
    }

    @Override
    public void delete(final String name) {
        ingredients.remove(name);
    }

    @Override
    public Ingredient get(final String name) {
        return ingredients.get(name);
    }

    @Override
    public Map<String, Ingredient> getAll() {
        return ingredients;
    }

    @Override
    public Map<String, Unit> getUnits() {
        return units;
    }

    private void IngredientsInitialisation() {
        List<Ingredient> ingredientsList = new ArrayList<>();
        if (units.isEmpty()) {
            return;
        }

        ingredientsList.add(new Ingredient("bacon", 16f, 3f, 5f, units.get("slice")));
        ingredientsList.add(new Ingredient("beaten eggs", 20f, 10f, 15f, units.get("")));
        ingredientsList.add(new Ingredient("butter, divided", 3f, 1.5f, 2.75f, units.get("tablespoon")));
        ingredientsList.add(new Ingredient("chopped green bell pepper", 0.5f, 0.25f, 0.375f, units.get("cup")));
        ingredientsList.add(new Ingredient("chopped fresh tomato", 0.5f, 0.25f, 0.375f, units.get("cup")));
        ingredientsList.add(new Ingredient("chopped onion", 0.5f, 0.25f, 0.5f, units.get("cup")));
        ingredientsList.add(new Ingredient("chopped red bell pepper", 0.5f, 0.25f, 0.375f, units.get("cup")));
        ingredientsList.add(new Ingredient("chopped red onion", 0.5f, 0.25f, 0.375f, units.get("cup")));
        ingredientsList.add(new Ingredient("dried basil", 0.25f, 0.125f, 0.375f, units.get("teaspoon")));
        ingredientsList.add(new Ingredient("dried oregano", 0.25f, 0.125f, 0.375f, units.get("teaspoon")));
        ingredientsList.add(new Ingredient("garlic powder", 0.5f, 0.25f, 0.375f, units.get("teaspoon")));
        ingredientsList.add(new Ingredient("ground pork sausage", 16f, 8f, 12f, units.get("ounce")));
        ingredientsList.add(new Ingredient("eggs", 12f, 6f, 9f, units.get("")));
        ingredientsList.add(new Ingredient("heavy whipping cream", 0.5f, 0.25f, 0.375f, units.get("cup")));
        ingredientsList.add(new Ingredient("lightly beaten eggs", 10f, 5f, 8f, units.get("")));
        ingredientsList.add(new Ingredient("onion powder", 0.5f, 0.25f, 0.375f, units.get("teaspoon")));
        ingredientsList.add(new Ingredient("pork sausage links", 10f, 5f, 8f, units.get("link")));
        ingredientsList.add(new Ingredient("prepared pizza crust", 2f, 1f, 2f, units.get("")));
        ingredientsList.add(new Ingredient("refrigerated crescent roll dough (such as Pillsbury)", 16f, 8f, 12f,
                units.get("ounce")));
        ingredientsList.add(new Ingredient("ricotta cheese", 2f, 1f, 1.5f, units.get("cup")));
        ingredientsList.add(new Ingredient("salsa", 1f, 0.5f, 0.75f, units.get("cup")));
        ingredientsList.add(new Ingredient("sausage links, sliced into 1/2-inch pieces", 4f, 2f, 3f, units.get("")));
        ingredientsList.add(new Ingredient("shredded Cheddar cheese", 4f, 2f, 3f, units.get("cup")));
        ingredientsList.add(new Ingredient("shredded mild Cheddar cheese", 0.5f, 0.25f, 0.375f, units.get("cup")));
        ingredientsList.add(new Ingredient("shredded mozzarella cheese", 2f, 1f, 1.5f, units.get("ounce")));
        ingredientsList.add(new Ingredient("tomato puree", 0.66f, 0.33f, 0.165f, units.get("cup")));

        ingredients = ingredientsList.stream().collect(
                Collectors.toMap(Ingredient::getName, Function.identity(), (a, b) -> a, TreeMap::new));
    }

    private void UnitsInitialisation() {
        List<Unit> unitsList = new ArrayList<>();

        unitsList.add(new Unit("", "", Unit.UnitType.EMPTY));
        unitsList.add(new Unit("cup", "cups", Unit.UnitType.FLOAT));
        unitsList.add(new Unit("link", "links", Unit.UnitType.INTEGER));
        unitsList.add(new Unit("ounce", "ounces", Unit.UnitType.FLOAT));
        unitsList.add(new Unit("slice", "slices", Unit.UnitType.INTEGER));
        unitsList.add(new Unit("tablespoon", "tablespoons", Unit.UnitType.FLOAT));
        unitsList.add(new Unit("teaspoon", "teaspoons", Unit.UnitType.FLOAT));

        units = unitsList.stream()
                .collect(Collectors.toMap(Unit::getNameInSingular, Function.identity(), (a,b) -> a, TreeMap::new));
    }

    private Map<String, Ingredient> ingredients;
    private Map<String, Unit> units;
}
