package com.mycompany;

import java.io.BufferedReader;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.StrictMath.min;
import static junit.framework.TestCase.assertTrue;

public class PizzaOrderService extends Service {
    public PizzaOrderService(final BufferedReader bufferedReader) {
        orderPizzaView = new OrderPizzaView(bufferedReader);

        chief = new Chief(this);
        chiefThread = new Thread(chief);
        chiefThread.start();
    }

    public void takeOrder() {
        String clientName = orderPizzaView.inputClientName();
        clientsNumber = 1;
        final boolean isRandom = false;
        Order order = makeOrder(clientName, isRandom);
        chief.takeOrder(order);
    }

    public void emulateClientsSteam() {
        clientsNumber = orderPizzaView.inputClientsNumber();
        emulateClientsSteam(clientsNumber);
    }

    public void chiefGotOrder(int number) {
        orderPizzaView.print("Chief has got order #" + Integer.toString(number + 1) + ".");
    }

    public void giveOrder(final Order order, int orderSize) {
        orderPizzaView.showOrder(order);
        orderPizzaView.print("Orders number in the query:" + orderSize);
        orderPizzaView.print("");
        if (clientsNumber == order.getNumber() + 1) {
            fireEvent(Events.SHOW_MENU);
        }
    }

    public void clientMadeOrder(final String clientName) {
        orderPizzaView.print("Client " + clientName + " has made an order.");
    }

    public void close() {
        chiefThread.interrupt();
    }

    private Order makeOrder(final String clientName, boolean isRandom) {
        Pizza pizza = new Pizza("Composite pizza");
        Map<String, Ingredient> newIngredients = chooseIngredients(isRandom);
        pizza.setIngredients(newIngredients);
        pizza.setSize(orderPizzaView.inputPizzaSize(isRandom));
        return new Order(clientName, pizza);
    }

    private synchronized Map<String, Ingredient> chooseIngredients(boolean isRandom) {
        Map<String, Ingredient> newIngredients = new TreeMap<>();

        Map<String, Ingredient> existingIngredients = IngredientRepositoryFactory.get().getAll();

        List<String> ingredientsNames = getNonZerosIngredientsNames(existingIngredients);
        if (ingredientsNames.size() == 0) {
            return newIngredients;
        }

        int minIngredientsNumber = min(ingredientsNames.size(), MIN_INGREDIENTS_NUMBER);
        int maxIngredientsNumber = (!isRandom) ? MAX_INGREDIENTS_NUMBER :
                getIngredientsNumberRandom(ingredientsNames.size());
        do {
            chooseIngredient(existingIngredients, ingredientsNames, newIngredients, isRandom);

            ingredientsNames = getNonZerosIngredientsNames(existingIngredients);
        } while (willContinue(newIngredients, ingredientsNames,
                minIngredientsNumber, maxIngredientsNumber, isRandom));

        return newIngredients;
    }

    private void chooseIngredient(final Map<String, Ingredient> existingIngredients,
                                  final List<String> ingredientsNames,
                                  final Map<String, Ingredient> newIngredients,
                                  boolean isRandom) {

        if (!isRandom) {
            orderPizzaView.showIngredients(ingredientsNames);
        }

        String ingredientName = (!isRandom) ? orderPizzaView.inputIngredient(ingredientsNames) :
                orderPizzaView.inputIngredientRandom(ingredientsNames);
        Ingredient ingredientFrom = existingIngredients.get(ingredientName);

        float quantity = separateIngredient(ingredientFrom, newIngredients, isRandom);
        addQuantity(ingredientFrom, quantity, newIngredients);
        subtractQuantity(ingredientFrom, quantity, existingIngredients);
    }

    private float separateIngredient(final Ingredient ingredientFrom,
                                     final Map<String, Ingredient> toIngredients,
                                     boolean isRandom) {
        String name = ingredientFrom.getName();
        float currentQuantity = (toIngredients.get(name) == null) ? 0f : toIngredients.get(name).getQuantity();

        float newQuantity = (!isRandom) ? getIngredientQuantity(ingredientFrom, currentQuantity) :
                getIngredientQuantityRandom(ingredientFrom, currentQuantity);
        return newQuantity;
    }

    private void addQuantity(final Ingredient ingredientFrom, float addQuantity,
                             final Map<String, Ingredient> newIngredients) {
        String name = ingredientFrom.getName();
        if (newIngredients.get(name) == null) {
            float minQuantityPerPizza = ingredientFrom.getMinQuantityPerPizza();
            float maxQuantityPerPizza = ingredientFrom.getMaxQuantityPerPizza();
            Unit unit = ingredientFrom.getUnit();
            newIngredients.put(name,
                    new Ingredient(name, addQuantity, minQuantityPerPizza, maxQuantityPerPizza, unit));
        } else {
            Ingredient ingredient = newIngredients.get(name);
            if (ingredient.getQuantity() + addQuantity == 0f) {
                newIngredients.remove(name);
            } else {
                ingredient.add(addQuantity);
            }
        }
    }

    private void subtractQuantity(final Ingredient ingredientFrom, float subtrahendQuantity,
                                  final Map<String, Ingredient> existingIngredients) {
        ingredientFrom.add(-subtrahendQuantity);
        IngredientRepository ingredientRepository = IngredientRepositoryFactory.get();
        if (ingredientFrom.getQuantity() == 0f) {
            ingredientRepository.delete(ingredientFrom.getName());
            existingIngredients.remove(ingredientFrom.getName());
        } else {
            ingredientRepository.update(ingredientFrom);
        }
    }

    private List<String> getNonZerosIngredientsNames(final Map<String, Ingredient> ingredients) {
        List<String> ingredientsNames = new ArrayList<>();
        ingredients.values().forEach(ingredient -> {
            if (ingredient.getQuantity() >= ingredient.getMinQuantityPerPizza()) {
                ingredientsNames.add(ingredient.getName());
            }
        });
        return ingredientsNames;
    }

    private int getIngredientsNumberRandom(int ingredientsNumber) {
        int minIngredientsNumber = Integer.min(ingredientsNumber, MIN_INGREDIENTS_NUMBER);
        int maxIngredientsNumber = Integer.min(ingredientsNumber, MAX_INGREDIENTS_NUMBER);
        return minIngredientsNumber + (new Random()).nextInt(maxIngredientsNumber - minIngredientsNumber + 1);
    }

    private float getIngredientQuantity(final Ingredient ingredientFrom, float currentQuantity) {
        float minQuantityValue = ingredientFrom.getMinQuantityPerPizza();
        float maxQuantityValue = min(ingredientFrom.getQuantity() + currentQuantity,
                ingredientFrom.getMaxQuantityPerPizza());
        float quantity = orderPizzaView.inputIngredientQuantity(
                ingredientFrom, currentQuantity, minQuantityValue, maxQuantityValue);

        return quantity;
    }

    private float getIngredientQuantityRandom(Ingredient ingredientFrom, float currentQuantity) {
        assertTrue(ingredientFrom.getQuantity() + currentQuantity >= ingredientFrom.getMinQuantityPerPizza());

        Unit unit = ingredientFrom.getUnit();
        float minQuantityValue = ingredientFrom.getMinQuantityPerPizza();
        float maxQuantityValue = min(ingredientFrom.getQuantity(), ingredientFrom.getMaxQuantityPerPizza());

        float quantity;
        int attemptNumber = 0;
        final int ATTEMPTS_NUMBER = 10;
        do {
            if (unit.getType() == Unit.UnitType.INTEGER || unit.getType() == Unit.UnitType.EMPTY) {
                quantity = minQuantityValue + (new Random()).nextInt((int)Math.floor(
                        maxQuantityValue - minQuantityValue) + 1);
            } else {
                quantity = minQuantityValue + (new Random()).nextFloat() * (maxQuantityValue - minQuantityValue);
            }
            ++attemptNumber;

            if (attemptNumber >= ATTEMPTS_NUMBER) {
                log.log(Level.SEVERE, "Couldn't take the ingredient: " + ingredientFrom.getName() + " " +
                        Float.toString(ingredientFrom.getQuantity()) + " " +
                        ingredientFrom.getUnit().getNameInSingular());
                break;
            }
        } while (quantity == 0f);
        return quantity;
    }

    private boolean willContinue(final Map<String, Ingredient> newIngredients,
                                 final List<String> ingredientsNames,
                                 int minIngredientsNumber,
                                 int maxIngredientsNumber,
                                 boolean isRandom) {

        if (ingredientsNames.size() == 0 || newIngredients.size() >= maxIngredientsNumber) {
            return false;
        }

        return (newIngredients.size() < minIngredientsNumber) ||
                (isRandom || orderPizzaView.inputWillContinue());
    }

    private void emulateClientsSteam(int clientsNumber) {
        final boolean isRandom = true;

        Map<String, Ingredient> existingIngredients = IngredientRepositoryFactory.get().getAll();
        orderPizzaView.showIngredients(existingIngredients);

        String clientName = "client1";
        Order order = makeOrder(clientName, isRandom);
        clientMadeOrder(clientName);
        chief.takeOrder(order);

        try {
            for (int clientNumber = 1; clientNumber < clientsNumber; ++clientNumber) {
                TimeUnit.MILLISECONDS.sleep((new Random()).nextInt(MAX_TIME_BETWEEN_TWO_ORDERS));

                orderPizzaView.showIngredients(existingIngredients);

                clientName = "client" + (clientNumber + 1);
                order = makeOrder(clientName, isRandom);
                clientMadeOrder(clientName);
                chief.takeOrder(order);
            }
        } catch (InterruptedException exception) {
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    private OrderPizzaView orderPizzaView;
    private Chief chief;
    private Thread chiefThread;
    private int clientsNumber;
    private static final int MIN_INGREDIENTS_NUMBER = 2;
    private static final int MAX_INGREDIENTS_NUMBER = 8;
    private final int MAX_TIME_BETWEEN_TWO_ORDERS = 2_000;   // milliseconds
    private static Logger log = Logger.getLogger(PizzaOrderService.class.getName());
}
