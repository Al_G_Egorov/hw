package com.mycompany;

import static org.junit.Assert.assertTrue;

/**
 * This class keeps information about ingredients.
 * The constructor is private. For create objects the load method are used.
 */
public class Ingredient implements Cloneable, java.io.Serializable {
    public Ingredient(final String name, float quantity, float minQuantityPerPizza,
                      float maxQuantityPerPizza, final Unit unit) {
        this.name = name;
        this.quantity = quantity;
        this.minQuantityPerPizza = minQuantityPerPizza;
        this.maxQuantityPerPizza = maxQuantityPerPizza;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public float getQuantity() {
        return quantity;
    }

    public float getMinQuantityPerPizza() {
        return minQuantityPerPizza;
    }

    public float getMaxQuantityPerPizza() {
        return maxQuantityPerPizza;
    }

    public float add(float addedQuantity) {
        assertTrue(quantity + addedQuantity >= 0);
        quantity = quantity + addedQuantity;
        return quantity;
    }

    public float add(final Ingredient ingredient) {
        return add(ingredient.getQuantity());
    }

    public Unit getUnit() {
        return unit;
    }

    public Ingredient clone() throws CloneNotSupportedException {
        Ingredient newIngredient = (Ingredient) super.clone();
        newIngredient.unit = unit.clone();
        return newIngredient;
    }

    private String name;
    private float quantity;
    private float minQuantityPerPizza;                  // minimum recommended quantity per a pizza.
    private float maxQuantityPerPizza;                  // maximum recommended quantity per a pizza.
    private Unit unit;
}
