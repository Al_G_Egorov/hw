package com.mycompany;

import java.io.IOException;
import java.util.logging.LogManager;

public class App {
    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logging.properties"));
        } catch (IOException exception) {
            System.err.println("Could not setup logger configuration: " + exception.toString());
        }

        PizzeriaLauncher.start();
    }
}
