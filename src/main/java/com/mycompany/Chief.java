package com.mycompany;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Chief implements Runnable {
    Chief() {
        jobs = new ArrayList<>();
    }

    Chief(final PizzaOrderService pizzaOrderService) {
        jobs = new ArrayList<>();
        this.pizzaOrderService = pizzaOrderService;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Order order = (Order) getJob();
                if (pizzaOrderService != null) {
                    pizzaOrderService.chiefGotOrder(order.getNumber());
                }
                Thread thread = new Thread(order);
                thread.start();
                thread.join();
                if (pizzaOrderService != null) {
                    pizzaOrderService.giveOrder(order, jobs.size());
                }
            } catch (InterruptedException exception) {
                log.log(Level.SEVERE, "Exception:", exception);
                Thread.currentThread().interrupt();
            }
        }
    }

    synchronized void takeOrder(Runnable job) {
        jobs.add(job);
        this.notifyAll();
    }

    private synchronized Runnable getJob() throws InterruptedException {
        while (jobs.size() == 0) {
            this.wait();
        }

        return jobs.remove(0);
    }

    private PizzaOrderService pizzaOrderService;
    private List<Runnable> jobs;
    private static Logger log = Logger.getLogger(Chief.class.getName());
}