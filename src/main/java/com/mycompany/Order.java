package com.mycompany;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Order implements Runnable {
    Order(final String clientName, final Pizza pizza) {
        number = ordersNumber;
        ++ordersNumber;
        this.clientName = clientName;
        this.pizza = pizza;
    }

    @Override
    public void run() {
        try {
            TimeUnit.MILLISECONDS.sleep(TIME_ORDER);
        } catch (InterruptedException exception) {
            log.log(Level.SEVERE, "Exception:", exception);
        }
    }

    public int getNumber() {
        return number;
    }

    public String getClientName() {
        return clientName;
    }

    public Pizza getPizza() {
        return pizza;
    }

    private final String clientName;
    private final int number;
    private static int ordersNumber = 0;
    private Pizza pizza;                            // a returned object.
    private static final int TIME_ORDER = 2_000;                 // milliseconds
    private static Logger log = Logger.getLogger(Order.class.getName());
}
