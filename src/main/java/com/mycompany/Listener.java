package com.mycompany;

public interface Listener {
    void onEvent(Events event);
}
