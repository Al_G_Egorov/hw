package com.mycompany;

import java.util.Map;

public interface IngredientRepository {
    void insert(final Ingredient ingredient);

    void update(final Ingredient ingredient);

    void delete(final String name);

    Ingredient get(final String name);

    Map<String, Ingredient> getAll();

    Map<String, Unit> getUnits();
}
