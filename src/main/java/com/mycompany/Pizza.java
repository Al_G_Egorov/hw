package com.mycompany;

import java.util.*;

public class Pizza {
    public Pizza(final String name) {
        this.name = name;
        ingredients = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Map<String, Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<String, Ingredient> ingredients) {
        for (Ingredient ingredient : ingredients.values()) {
            this.ingredients.put(ingredient.getName(), ingredient);
        }
    }

    public void addIngredient(Ingredient ingredient) {
        if (ingredients.keySet().contains((ingredient.getName()))) {
            ingredients.get(ingredient.getName()).add(ingredient);
        } else {
            this.ingredients.put(ingredient.getName(), ingredient);
        }
    }

    public Pizza clone() throws CloneNotSupportedException {
        Pizza newPizza = (Pizza) super.clone();
        Map<String, Ingredient> newIngredients = new TreeMap<>();
        for (Ingredient ingredient : ingredients.values()) {
            newIngredients.put(ingredient.getName(), ingredient.clone());
        }
        newPizza.ingredients = newIngredients;
        return newPizza;
    }

    private String name;
    private int size;
    private Map<String, Ingredient> ingredients;
}
