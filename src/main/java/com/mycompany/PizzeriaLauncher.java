package com.mycompany;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PizzeriaLauncher {
    public static void start() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        PizzaOrderService pizzaOrderService = new PizzaOrderService(bufferedReader);
        InputIngredientsService inputIngredientsService = new InputIngredientsService(bufferedReader);


        MenuView menuView = new MenuView(bufferedReader);
        MenuController menuController = new MenuController(menuView, pizzaOrderService,
                inputIngredientsService);


        menuView.addEventListener(menuController);
        pizzaOrderService.addEventListener(menuController);
        inputIngredientsService.addEventListener(menuController);

        menuController.onEvent(Events.SHOW_MENU);
    }
}
