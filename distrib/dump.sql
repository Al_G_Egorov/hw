--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: unit_type; Type: TYPE; Schema: public; Owner: alex
--

CREATE TYPE unit_type AS ENUM (
    'empty',
    'integer',
    'float'
);


ALTER TYPE unit_type OWNER TO alex;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ingredients; Type: TABLE; Schema: public; Owner: alex
--

CREATE TABLE ingredients (
    id integer NOT NULL,
    name character varying(80) NOT NULL,
    quantity real DEFAULT 0.0 NOT NULL,
    min_quantity_per_pizza real DEFAULT 0.0 NOT NULL,
    max_quantity_per_pizza real DEFAULT 1.0 NOT NULL,
    unit_id integer,
    CONSTRAINT ingredients_check CHECK (((min_quantity_per_pizza >= (0)::double precision) AND (min_quantity_per_pizza < max_quantity_per_pizza))),
    CONSTRAINT ingredients_check1 CHECK (((max_quantity_per_pizza >= (0)::double precision) AND (min_quantity_per_pizza < max_quantity_per_pizza))),
    CONSTRAINT ingredients_quantity_check CHECK ((quantity >= (0)::double precision))
);


ALTER TABLE ingredients OWNER TO alex;

--
-- Name: ingredients_id_seq; Type: SEQUENCE; Schema: public; Owner: alex
--

CREATE SEQUENCE ingredients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredients_id_seq OWNER TO alex;

--
-- Name: ingredients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alex
--

ALTER SEQUENCE ingredients_id_seq OWNED BY ingredients.id;


--
-- Name: units; Type: TABLE; Schema: public; Owner: alex
--

CREATE TABLE units (
    id integer NOT NULL,
    name_in_singular character varying(80) NOT NULL,
    name_in_plural character varying(80) DEFAULT ''::character varying NOT NULL,
    type unit_type DEFAULT 'empty'::unit_type
);


ALTER TABLE units OWNER TO alex;

--
-- Name: units_id_seq; Type: SEQUENCE; Schema: public; Owner: alex
--

CREATE SEQUENCE units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE units_id_seq OWNER TO alex;

--
-- Name: units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: alex
--

ALTER SEQUENCE units_id_seq OWNED BY units.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alex
--

ALTER TABLE ONLY ingredients ALTER COLUMN id SET DEFAULT nextval('ingredients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: alex
--

ALTER TABLE ONLY units ALTER COLUMN id SET DEFAULT nextval('units_id_seq'::regclass);


--
-- Data for Name: ingredients; Type: TABLE DATA; Schema: public; Owner: alex
--

COPY ingredients (id, name, quantity, min_quantity_per_pizza, max_quantity_per_pizza, unit_id) FROM stdin;
1	bacon	16	3	5	5
2	beaten eggs	20	10	15	1
3	butter, divided	3	1.5	2.75	6
4	chopped green bell pepper	0.5	0.25	0.375	2
5	chopped fresh tomato	0.5	0.25	0.375	2
6	chopped onion	0.5	0.25	0.5	2
7	chopped red bell pepper	0.5	0.25	0.375	2
8	chopped red onion	0.5	0.25	0.375	2
9	dried basil	0.25	0.125	0.375	7
10	dried oregano	0.25	0.125	0.375	7
11	garlic powder	0.5	0.25	0.375	7
12	ground pork sausage	16	8	12	4
13	eggs	12	6	9	1
14	heavy whipping cream	0.5	0.25	0.375	2
15	lightly beaten eggs	10	5	8	1
16	onion powder	0.5	0.25	0.375	7
17	pork sausage links	10	5	8	3
18	prepared pizza crust	2	1	2	1
19	refrigerated crescent roll dough (such as Pillsbury)	16	8	12	4
20	ricotta cheese	2	1	1.5	2
21	salsa	1	0.5	0.75	2
22	sausage links, sliced into 1/2-inch pieces	4	2	3	1
23	shredded Cheddar cheese	4	2	3	2
24	shredded mild Cheddar cheese	0.5	0.25	0.375	2
25	shredded mozzarella cheese	2	1	1.5	4
26	tomato puree	0.660000026	0.330000013	0.495000005	2
\.


--
-- Name: ingredients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alex
--

SELECT pg_catalog.setval('ingredients_id_seq', 26, true);


--
-- Data for Name: units; Type: TABLE DATA; Schema: public; Owner: alex
--

COPY units (id, name_in_singular, name_in_plural, type) FROM stdin;
1			empty
2	cup	cups	float
3	link	links	integer
4	ounce	ounces	float
5	slice	slices	integer
6	tablespoon	tablespoons	float
7	teaspoon	teaspoons	float
\.


--
-- Name: units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: alex
--

SELECT pg_catalog.setval('units_id_seq', 7, true);


--
-- Name: ingredients_pkey; Type: CONSTRAINT; Schema: public; Owner: alex
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_pkey PRIMARY KEY (id);


--
-- Name: units_name_in_singular_key; Type: CONSTRAINT; Schema: public; Owner: alex
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_name_in_singular_key UNIQUE (name_in_singular);


--
-- Name: units_pkey; Type: CONSTRAINT; Schema: public; Owner: alex
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: ingredients_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: alex
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

